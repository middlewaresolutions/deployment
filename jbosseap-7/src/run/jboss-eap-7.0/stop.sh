#!/bin/sh
JBOSS_HOME=$(pwd)
JBOSS_REMOTE_PORT=${jboss.port.management}

JBOSS_RUNNING=$(ps -ef | grep jboss.socket.binding.port-offset=${jboss.offset} | grep -v grep | wc -l)
 
if [ $JBOSS_RUNNING -gt 0 ]; then
    echo "Stopping JBoss..."
    # stop without offset
    $JBOSS_HOME/bin/jboss-cli.sh --controller=localhost:$JBOSS_REMOTE_PORT --connect --command=:shutdown

    echo "JBoss stop."
else
    echo "JBoss in $JBOSS_HOME is still stopped."
fi