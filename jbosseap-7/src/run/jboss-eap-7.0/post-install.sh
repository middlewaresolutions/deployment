#!/bin/sh
CURRENT_DIR=$(pwd)

echo "Start JBoss before patching"
./start.sh &
sleep 30s

echo "Patch to 7.0.8"
./bin/jboss-cli.sh --connect --controller=localhost:${jboss.port.management} --file=patch.txt

echo "Stop JBoss after patching"
./stop.sh
