## Description ##

Ce référentiel regroupe des projets MAVEN pour faciliter le déploiement des application WAR.

## Objectifs ##

Ces projets MAVEN sont pour objectifs de :

1. Préparer un serveur JBoss "prêt à exécuter",
2. Permettre le paramétrage par environnement,
3. Installer et exécuter les serveurs,
4. Offrir à chaque application un cadre souple,
5. Réduire l'adhérence entre applications.

## Vision globale ##

Le shéma suivant présente les mécanismes mis en jeu.

Le résultat attendu est un serveur JBoss configuré, déployé et en état de fonctionnement.
Pour cela, il est nécessaire d'avoir:

* un ou plusieurs fichiers WAR (l'application),
* un modèle de serveur JBoss,
* des fichiers de paramétrages des environnements.

Avec ces éléments, ces projets de déploiements :

1. réalisent la construction du serveur JBoss adapté à l'environnement cible,
2. déploie le serveur sur une VM existante via SSH,
3. arrête l'ancienne version et lance la nouvelle.

![Vision globale](https://bitbucket.org/middlewaresolutions/deployment/wiki/DeploiementJBoss/Diapositive1.JPG "Vision globale")

## Documentation ##

La documentation complète est mise à disposition dans le [wiki](https://bitbucket.org/middlewaresolutions/deployment/wiki/Home).

## Prise en main rapide ##

Pour une prise en main rapide, il convient de :

1. Récupérer le projet
2. Définir l'environnement local

Chaque tâche est exécutée pour un environnement précis.
Il est nécessaire de configurer l'environnement "local".
Pour cela:

* il faut disposer un serveur SSH qui recevra les serveurs.
* préciser quel est son nom ou son IP
* préciser le chemin local au serveur où seront déployées les applications (ici les templates de serveurs).

3. Construire le projet racine

~~~~ shell
mvn install -Denvironment=local
~~~~

4. Définir un projet de déploiement en utilisant l'archetype

~~~~ shell
mvn ...
~~~~