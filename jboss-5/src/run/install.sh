#!/bin/sh
CURRENT_DIR=$(pwd)

# Stop old server
if [ -d "latest" ]; then
    echo "Stop Server"
    cd latest
    ./stop.sh

    # Check stop JBoss²
    if [ $? -eq 0 ]
    then
      echo "Server stopped"
    else
      echo "Error when Server stoped" >&2
    fi

    cd ..
    rm latest
fi

# If deploy same version, dir is deleted
if [ -d "${project.build.finalName}" ]; then
    echo "Same version exist, it is deleted."
    rm -rf ${project.build.finalName}
fi

# unzip new version
echo "Unzip Server in $CURRENT_DIR"
unzip ${project.build.finalName}-run -d ${project.build.finalName}

# Recreate link to lastest
ln -s ${project.build.finalName} latest

# Configure JBoss
echo "Configure new version"
cd latest
# set execute mode
chmod -R u+x *.sh
if [ -d "bin" ]; then
  chmod -R u+x bin/*.sh
fi

# Check start JBoss
if [ $? -eq 0 ]
then
  echo "Successfully installed"
  exit 0
else
  echo "Problem occured: " >&2
  exit 1
fi
