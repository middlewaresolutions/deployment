#!/bin/sh
#$JBOSS_HOME/bin/shutdown.sh --server=${MACHINE}:${JBOSS_PORT_ARRET} -S  >${LOG_TP}/tpjbmal_arret.log 2>> ${LOG_TP}/tpjbmal_arret.log

JBOSS_HOME=$(pwd)
JBOSS_REMOTE_PORT=${jboss.port.management}

JBOSS_RUNNING=$(ps -ef | grep "Djboss.home.dir=$JBOSS_HOME" | grep -v grep | wc -l)
 
if [ $JBOSS_RUNNING -gt 0 ]; then
    echo "Stopping JBoss..."
    # stop without offset
    $JBOSS_HOME/bin/shutdown.sh --server=localhost:$JBOSS_REMOTE_PORT -S --user=${jboss.admin.login} --password=${jboss.admin.pwd}

    echo "JBoss stop."
else
    echo "JBoss in $JBOSS_HOME is still stopped."
fi