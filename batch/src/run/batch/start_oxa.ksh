#!/bin/ksh 
#============================================================================
#@(#) Procédure   :  lancer.ksh
#@(#) Description :  Script de lancement des batchs PENHIR2
#@(#)
#@(#) Création    :  01/10/2014
#@(#) Paramètres  :  1 - REPRISE = Step de reprise dans le traitement
#@(#)                2 - FIN     = Dernier step dans le traitement
#@(#)                3..n  - Paramètres fonctionnels du batch, voir DEX
#@(#)
#@(#) Historique  :  03/10/2014 [B. Florat] Script rendu generique, logging 
#@(#)                de stderr
#@(#)                16/12/2014 : [P. Tonnerre] version ENIM avec sorties OXA
#============================================================================

#==================================================
# = OXA ( Parametrage des transferts automatiques )
export OXALYS_LOG=O
export OXALYS_ASA=O
export OXALYS_NASA=O
export OUTIL_OXA_KSH="/prod/outil/batch/oxa/ksh"

export REPLOG=${REPLOG}
export REPLOGCTRL=${REPLOGCTRL}

# Note : les variables en ${_nom_} sont substituées par maven au build

#........................................................................
# Functions
function usage {
  print "Usage : ${SCRIPT} <balise début ou ''> <balise fin ou ''> ${ARGS_ATTENDUS}"
} 

function tst_parms {
 if [[ ${1} -ne ${NB_ARGS_ATTENDUS} ]]; then
  usage
  echo "ETAT:KO" >> ${LOGCTRL}
  rstlog
  oxa_msg_err "${CMD}" 2
 fi
} # tst_parms

function tst_balises {
  (( i=1 )) ; (( FLG_ERR = 0 ))
  nbparms=${#@}
  while (( ${i} <= ${nbparms} )); do
    tst_balise $1
    shift
    (( i += 1 ))
  done
  if [[ ${FLG_ERR} -eq 1 ]]; then
    echo "ETAT:KO" >> ${LOGCTRL}
    rstlog
    oxa_msg_err "Balises autorisées : debut fin" 2
  fi
}

function tst_balise {
  if [[ "$1" != @(debut|traitement|fin) ]]; then
    oxa_msg_warn "Balise $1 incorrecte"
    (( FLG_ERR = 1 ))
  fi
}

function nextstep {
  if [[ ${REPRISE} == ${FIN} ]]; then
    REPRISE=fin
  else
    REPRISE=$1
  fi
} # nextstep

# Cette fonction contruit l'argument -Darg1=value1 -Darg2=value2 ...
function construireDArgs {
 ((comp = 2))
 for arg in ${ARGS_ATTENDUS[*]}; do
   D_ARGS="${D_ARGS} -D${arg}=${ARGS_AS_ARRAY[$comp]}" 
   ((comp+=1))
 done
} # construireDArgs

function rstlog {
  exec 1>&5 2>&6 ; exec 5>&- 6>&- ; cat $LOG
}

########################################################################
# Main () 
########################################################################

#------------------------------------------------------------------------
# PROD - Initialisation variables script génériques production
#        initialisation système Oxalog et gestion des E/S
#------------------------------------------------------------------------
SERVER=`hostname`
LADATE=`date +%Y%m%d_%H%M%S`
SCRIPT=$(basename $0)
CLEF=$(basename $0 .ksh )
REPRISE=${1:-debut}
FIN=${2:-fin}
PID=$$
(( RC=0 ))

## A quoi cela sert-il ??
. ./envbatch.sh

LOG=${REPLOG}/${SCRIPT%.*}_${PID}_${LADATE}.log
LOGCTRL=${REPLOGCTRL}/${SCRIPT%.*}_${SERVER}.log

> ${LOG}
> ${LOGCTRL}

print "${LOG}" >> ${LOGCTRL}

# PROD - Sauvegarde des sorties std et stderr dans les fd 5 et 6
exec 5>&1 6>&2 ; exec >> ${LOG} 2>&1

# PROD - Initialisation Oxalog
. /prod/outil/batch/oxa/ksh/OXAG000.ksh

ARGS_AS_ARRAY=("$@")
ARGS_ATTENDUS="typeTraitement"
NB_ARGS_ATTENDUS=`echo "${ARGS_ATTENDUS}" |wc -w`
# Ajout de deux arguments pour prendre en compte les etiquettes de debut/fin
((NB_ARGS_ATTENDUS=${NB_ARGS_ATTENDUS} + 2))

#------------------------------------------------------------------------
# PROD - Initialisation variables script spécifiques production
#        et initialisation entrées OXA
#------------------------------------------------------------------------

# PROD - Initialisation de l'entête Oxa
oxa_debut
oxa_app ${DOMAINE} ${APPLICATION}
oxa_msg_info "PENHIR2 - Traitement batch ${SCRIPT}"
oxa_msg_info "Fichier de log ... ${LOG}"

#------------------------------------------------------------------------
# PROD - Contrôles préliminaires génériques script production
#------------------------------------------------------------------------
# Contrôle nombre paramètres reçus
CMD="${SCRIPT} ${*}"
(( PP = ${#} ))
tst_parms ${PP} 

# Contrôle balise(s) reçue(s) en paramètre(s)
tst_balises ${REPRISE} ${FIN}

#------------------------------------------------------------------------
# Paramètres de lancement
#------------------------------------------------------------------------

LIGNE_LANCEMENT="./start_java.sh"

#------------------------------------------------------------------------
# PROD - Boucle principale de traitement
#------------------------------------------------------------------------
while [[ ${REPRISE} != "fin" ]]
do

  oxa_msg_info "LABEL : ${REPRISE}"

  case ${REPRISE} in

    debut)
      # PROD
      tmpdate=$(date +'%d %m %Y %H:%M:%S')
      oxa_msg_info "===> Début de traitement"
      print "Date de debut:${tmpdate}" >> ${LOGCTRL}
      nextstep traitement
      ;;

    traitement)
      oxa_msg_info "===> Commande : ${LIGNE_LANCEMENT}"
      # Lancement effectif
      ### eval ${LIGNE_LANCEMENT} 2>&1 | tee -a ${LOG}
      eval ${LIGNE_LANCEMENT}
      # Gestion du code de retour et des logs
      (( RC=$? ))
      if (( ${RC} > 0 )); then
        oxa_msg_err "Erreur execution" ${RC}
      fi
      nextstep endtrt
      ;;

    endtrt)
      # PROD 
      oxa_msg_info "===> Fin de traitement"
      nextstep fin
      ;;

    *)
      # PROD 
      oxa_msg_err "+++ Etiquette invalide ${REPRISE}" 99
      REPRISE=fin
      ;;

   esac
done

#------------------------------------------------------------------------
# PROD - Initialisation footer OXA et gestion des E/S
#------------------------------------------------------------------------
oxa_msg_info "--------------------------------------------"
oxa_msg_info "             FIN ${SCRIPT%.*}              "

# Restauration des sorties std et stderr et affichage log pour VTOM
exec 1>&5 2>&6 ; exec 5>&- 6>&- ; cat $LOG

oxa_fin
