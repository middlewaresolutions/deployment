#!/bin/sh 

#==================================================
# = SUDO ( autorisation execution par rsh )
export sudo_root=""
export sudo_oracle=""
export sudo_sysload=""

#==================================================
# = ORACLE
export PATH_agent10g=""
export TNS_ADMIN=/prod/ppr/sgbd/oracl/tns/env

#==================================================
# = OXA ( Parametrage des transferts automatiques )
export OXALYS_LOG=O
export OXALYS_ASA=O
export OXALYS_NASA=O
export OUTIL_OXA_KSH="/prod/outil/batch/oxa/ksh"

#==================================================
# = TMP_TECH  ( Repertoire technique temporaire )
export TMP_TECH=/fictemp/tech
