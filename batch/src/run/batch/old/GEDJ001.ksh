#!/bin/ksh
#===================================================================================================
#@(#) Procedure   : GEDJ001.ksh 
#@(#) Description : Génération fichier feu vert et fichier des index (XML) pour les factures CHORUS PRO (au format PDF) téléchargées par l'utilitaire UCP
#@(#)               permettant de constituer un lot GED qui sera traité par le service BatchAgent qui assure l'injection en GED
#@(#) Creation    : 06/09/2017
#@(#) Historique  : 
#@(#)
#@(#) Parametres  :  1 - REPRISE = Step de reprise dans le traitement
#@(#)                2 - FIN = Dernier step dans le traitement
#===================================================================================================

#........................................................................
# Functions

function nextstep {
  if [[ "${REPRISE}" == "${FIN}" ]]; then
    REPRISE=fin
  else
    REPRISE=$1
  fi
} # nextstep

function rstlog {
  exec 1>&5 2>&6 ; exec 5>&- 6>&- ; cat $LOG
}

function recup_version {
  APP_VERSION=$(echo $(basename `ls -ltra ${APPLI_JOB_PATH}/${JOB_NAME_MINUS}_*.jar |tail -1  |awk '{print $9}'`) | sed 's/'${JOB_NAME_MINUS}'//' | sed 's/\.jar$//')
}

LADATE=$(date +'%Y%m%d_%H%M%S')
SCRIPT=$(basename $0)
REPRISE=${1:-debut}
FIN=${2:-fin}
PID=$$
RC=0

. ${PROJECT}/outil/batch/ged/facturecpp/env/envbatch

LOG=${REPLOG}/${SCRIPT%.*}_${PID}_${LADATE}.log

> ${LOG}

# PROD - Sauvegarde des sorties std et stderr dans les fd 5 et 6
exec 5>&1 6>&2 ; exec >> ${LOG} 2>&1

. ${OUTIL_OXA_KSH}/OXAG000.ksh

oxa_debut
oxa_app OUTIL GED_FACTURECPP

oxa_msg_info "Constitution lot GED pour injection des factures CHORUS PRO en GED"
oxa_msg_info "---------------------------------------------"

#parametres de la JVM
JAVA_OPTS="-Xms256m -Xmx512m"

# Parametres de lancement
#------------------------------------------------------------------------
LIGNE_LANCEMENT="${JAVA_VM}"

#chemin de la configuration JNDI

#chemin de la configuration LOG4J

#parametres : conf JNDI et logs
JAVA_OPTS="${JAVA_OPTS} -Doracle.net.tns_admin=${TNS_ADMIN} ${JAVA_ORACLE_CACHE}"  # PROD ajout exploitation
LIGNE_LANCEMENT="${LIGNE_LANCEMENT} ${JAVA_OPTS}"

#parametres du batch
JOB_NAME=facturecpp
JOB_NAME_MINUS=`echo ${JOB_NAME} | tr "[A-Z]" "[a-z]"`
APPLI_JOB_PATH=${APPLI_JOB_PATH}/${JOB_NAME}

#contexte Spring

#classloader
recup_version
BATCH_JAR=${APPLI_JOB_PATH}/${JOB_NAME_MINUS}${APP_VERSION}.jar
BATCH_CLASSPATH="${APPLI_JOB_PATH}/*"
BATCH_CLASSPATH="${BATCH_CLASSPATH}:${APPLI_LIB_PATH}/*"
LIGNE_LANCEMENT="${LIGNE_LANCEMENT} -classpath ${BATCH_CLASSPATH}"

#lanceur
JAVA_MAIN=${APP_PROJECT_NAME}.${JOB_NAME_MINUS}${APP_VERSION}.${JOB_NAME}
LIGNE_LANCEMENT="${LIGNE_LANCEMENT} ${JAVA_MAIN}"

#tache a lancer

BATCH_PARAM="--context_param confTechnique=${APPLI_ENV_PATH}/"
LIGNE_LANCEMENT="${LIGNE_LANCEMENT} ${BATCH_PARAM}"

while [ "${REPRISE}" != "fin" ]
do
  oxa_msg_info "LABEL : ${REPRISE}"

  case "${REPRISE}" in

  debut)
    nextstep verif1 
  ;;

  verif1)
    oxa_msg_info "===> Vérification fichiers à traiter"

    if [ ! -d ${OUTIL_GED_DATA} ]
    then
      rstlog
      oxa_msg_err "     Répertoire de factures : ${OUTIL_GED_DATA} INEXISTANT !" 1
    fi

    ls -lR ${OUTIL_GED_DATA}

    if [ `find ${OUTIL_GED_DATA} -type f | wc -l` -eq 0 ]
    then
      rstlog
      oxa_msg_err "     Répertoire de factures : ${OUTIL_GED_DATA} pas de fichier" 2
    fi

    nextstep lancement
  ;;

  lancement)
    oxa_msg_info "===> [${LIGNE_LANCEMENT}]"

    ${LIGNE_LANCEMENT}
    (( RC=$? ))

    if [[ $RC = 0 ]]
    then
      oxa_msg_info "     Traitement termine OK"
    else
      rstlog
      oxa_msg_err "     Traitement termine KO" ${RC}
    fi

    nextstep etat_rep_sor_ap
  ;;

  etat_rep_sor_ap)
    oxa_msg_info "===> Vérification fichiers traités"

    ls -lR ${OUTIL_GED_DATA}

    if [ `find ${OUTIL_GED_DATA} -type f | wc -l` -eq 0 ]
    then
      rstlog
      oxa_msg_err "     Répertoire de factures : ${OUTIL_GED_DATA} pas de fichier" 3
    fi

    nextstep fin
  ;;

  *)
    rstlog
    oxa_msg_err "+++ Etiquette invalide ${REPRISE}" 99
    REPRISE=fin
  ;;
  esac
done

oxa_msg_info "--------------------------------------------"
oxa_msg_info "             FIN ${SCRIPT%.*}"

rstlog

oxa_fin
