#!/bin/sh
# Script d'installation des batchs PENHIR2
# Usage : voir la fonction 'usage()'

#Arret sur erreur
set -e

function usage(){
        echo "[FATAL] Usage : ./installation_batch.sh <chemin absolu vers le tar.bz2 a installer>"
}

# Nom du repertoire contenant envbatch
REP_ENVBATCH="env"

# Nom du fichier contenant le parametrage du batch
FILE_ENVBATCH="envbatch"

# Chemin absolu du tar.bz2
CHEMIN_ARCHIVE=$1

# == Controles ==
if [ -z "$CHEMIN_ARCHIVE" ]; then
 usage
 exit 1
fi

if [ ! -f $CHEMIN_ARCHIVE ];then
 echo "[FATAL] Le package batch (tar.bz2) n'existe pas"
 exit 1
fi

# == Installation des libs et du ksh du batch ==

# Repertoire de base des batchs penh2
REP_BASE=$PROJECT/pens/batch/penh/

cd $REP_BASE

nom_batch=`basename $CHEMIN_ARCHIVE | awk -F '-' '{print $1}'`

# Suppression des dossiers lib et ksh
if [ -d  ${nom_batch} ];then
 echo "[INFO] Suppression des anciennes versions des libs et du ksh du batch"
 rm -rf ${nom_batch}/lib
 rm -rf ${nom_batch}/ksh
 rm -f ${nom_batch}/modifs.txt
fi

tar xjpvf $CHEMIN_ARCHIVE >/dev/null

# Renomme le lanceur du nom du batch
nom_ksh=PENH2`echo "$nom_batch" | tr '[:lower:]' '[:upper:]'`.ksh
mv $nom_batch/ksh/lancer.ksh $nom_batch/ksh/$nom_ksh

# preparation envbatch
if [ ! -d  ${nom_batch}/${REP_ENVBATCH} ]; then
 mkdir ${nom_batch}/${REP_ENVBATCH}
fi

if [ ! -f  ${nom_batch}/${REP_ENVBATCH}/${FILE_ENVBATCH} ]; then 
 echo "[WARN] Attention, ce batch ne possede pas de fichier ${FILE_ENVBATCH}, veuillez le créer avant de lancer le batch, voir le DEX et guide installation"
fi

# Fin
echo "[INFO] Installation effectuee avec succes"
