#!/bin/sh 

export ENVEXE_ENVTRT_NomCourt="ppr"
export ENVEXE_ENVTRT_NomLong="Pre-Production"

export PROJECT=/prod/ppr
export REP_LOG_OXALYS=/ficdata/oxalys/log

#==================================================
# = Execution elements techniques

export PROD_TECH_ENV="${PROJECT}/prod/batch/tech/env/"
export PROD_TECH_KSH="${PROJECT}/prod/batch/tech/ksh"
export PROD_TECH_PERL="${PROJECT}/prod/batch/tech/Perl"

#==================================================
