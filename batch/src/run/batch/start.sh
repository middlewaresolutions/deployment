#!/bin/sh
CURRENT_DIR=$(pwd)

echo "Nothing to start."
echo "Batch is started only with oxalog or directly."
echo " - start directly: start_java.sh"
echo " - start with OXALOG: start_oxa.sh"

